#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

extern "C" {
    #include "Utils/NativeUtils.h"
}

const sf::String KLIPPI =
"\
 __\n\
/  \\\n\
|  |\n\
@  @\n\
|| ||\n\
|| ||\n\
|\\_/|\n\
\\___/\n\
";

const sf::String INTRO_PARAGRAPH =
KLIPPI + "\n\n\
Hello " + getUsername() + "! I am Klippi!\n\
What would you like to do?\n\
\n\n\
C:\\> \
";

int main() {
    sf::RenderWindow window(sf::VideoMode(1280, 720), "Klippi", sf::Style::Close);
    window.setVerticalSyncEnabled(true);

    sf::Font MorePerfectDOSVGA;
    if (!MorePerfectDOSVGA.loadFromFile("Assets/Fonts/MorePerfectDOSVGA.ttf"))
        exit(1);

    sf::SoundBuffer floppyRead;
    if (!floppyRead.loadFromFile("Assets/Sounds/FloppyRead.wav"))
        exit(1);

    sf::SoundBuffer bootup;
    if (!bootup.loadFromFile("Assets/Sounds/Bootup.wav"))
        exit(1);

    sf::Text text;
    text.setFont(MorePerfectDOSVGA);
    text.setCharacterSize(24);
#if SFML_VERSION_MAJOR >= 2 && SFML_VERSION_MINOR >= 4
    text.setFillColor(sf::Color::White);
#else
    text.setColor(sf::Color::White);
#endif

    sf::Sound sound;
    sound.setBuffer(bootup);
    sound.play();

    sf::Clock clock;
    
    sf::String textBuffer;
    int currentCharacter = 0;
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::TextEntered)
                textBuffer += event.text.unicode;
        }

        if (sound.getPlayingOffset().asSeconds() > 14 ||
                sound.getStatus() == sf::SoundSource::Status::Stopped) {
            if (clock.getElapsedTime().asMilliseconds() >= 50 && 
                    currentCharacter < INTRO_PARAGRAPH.getSize()) {
                clock.restart();
                textBuffer += INTRO_PARAGRAPH[currentCharacter];
                currentCharacter += 1;
            }
        }

        window.clear(sf::Color::Black);
        text.setString(textBuffer);
        window.draw(text);
        window.display();
    }

    return 0;
}
